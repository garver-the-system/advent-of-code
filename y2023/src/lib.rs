pub fn print_section(header: &str, body: &str) {
	let separator = "==========";

	println!("{header}");
	println!("{separator}");
	println!("{body}");
	println!("{separator}\n");
}
