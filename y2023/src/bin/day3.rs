use y2023::*;

fn main() {
	let input = include_str!("../../input/d3/input.txt");

	print_section("Part 1", &part1(input));
	print_section("Part 2", &part2(input));
}

fn part1(input: &str) -> String {
	let lines: Vec<&str> = input.lines().collect();
	let symbols = scan_for_symbols(&lines);
	let num_queue = get_neighbors(&symbols, (lines[0].len(), lines.len()));
	let numbers = scan_for_numbers(&lines, num_queue);
	let sum = numbers.iter().sum::<usize>();
	return sum.to_string();
}

fn part2(input: &str) -> String {
	todo!()
}

fn scan_for_symbols(input: &Vec<&str>) -> Vec<(usize, usize)> {
	let non_symbols = ['.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

	let mut symbol_coords = Vec::new();

	let x_max = input[0].len();
	let y_max = input.len();

	for y in 0..y_max {
		let chars: Vec<char> = input[y].chars().collect();
		for x in 0..x_max {
			let c = chars[x];

			if !non_symbols.contains(&c) {
				symbol_coords.push((x, y));
			}
		}
	}

	return symbol_coords;
}

fn get_neighbors(coords: &Vec<(usize, usize)>, bounds: (usize, usize)) -> Vec<(usize, usize)> {
	let neighbors_relative: [(isize, isize); 8] = [
		(-1, -1),
		(-1, 0),
		(-1, 1),
		(0, -1),
		(0, 1),
		(1, -1),
		(1, 0),
		(1, 1),
	];

	let mut neighbors_list = Vec::new();

	for coord in coords {
		let mut local_neighbors: Vec<(usize, usize)> = neighbors_relative
			.iter()
			.filter_map(|rel| {
				if let (Some(new_x), Some(new_y)) = (
					coord.0.checked_add_signed(rel.0),
					coord.1.checked_add_signed(rel.1),
				) {
					if bounds_check(&(new_x, new_y), bounds) && !coords.contains(&(new_x, new_y)) {
						return Some((new_x, new_y));
					}
				}
				return None;
			})
			.filter(|coord| bounds_check(coord, bounds))
			.collect();
		neighbors_list.append(&mut local_neighbors);
	}

	neighbors_list.dedup();
	return neighbors_list;
}

fn bounds_check(coord: &(usize, usize), bounds: (usize, usize)) -> bool {
	if coord.0 < bounds.0 && coord.1 < bounds.1 {
		return true;
	}
	return false;
}

fn scan_for_numbers(input: &Vec<&str>, num_queue: Vec<(usize, usize)>) -> Vec<usize> {
	let digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

	let x_max = input[0].len();

	let mut char_2d = Vec::new();
	for line in input {
		let chars: Vec<char> = line.chars().collect();
		char_2d.push(chars);
	}

	let mut numbers = Vec::new();
	let mut seen = Vec::new();
	for (x, y) in num_queue {
		if !digits.contains(&char_2d[y][x]) || seen.contains(&(x, y)) {
			continue;
		}

		let mut cursor = x;
		while cursor > 0 && digits.contains(&char_2d[y][cursor - 1]) {
			cursor -= 1;
		}

		let mut num_str = String::new();
		while cursor < x_max && digits.contains(&char_2d[y][cursor]) {
			num_str.push(char_2d[y][cursor]);
			seen.push((cursor, y));
			cursor += 1;
		}

		numbers.push(num_str.parse::<usize>().unwrap());
	}

	return numbers;
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_part1() {
		let input = "\
			467..114..\n\
			...*......\n\
			..35..633.\n\
			......#...\n\
			617*......\n\
			.....+.58.\n\
			..592.....\n\
			......755.\n\
			...$.*....\n\
			.664.598..\
		";
		let expected = "4361";

		assert_eq!(expected, part1(input));
	}

	mod scan_for_symbols {
		use super::*;

		#[test]
		fn no_symbols() {
			let input = ".".lines().collect();
			let expected: Vec<(usize, usize)> = vec![];

			assert_eq!(expected, scan_for_symbols(&input));
		}

		#[test]
		fn only_symbols() {
			let input = "*".lines().collect();
			let expected: Vec<(usize, usize)> = vec![(0, 0)];

			assert_eq!(expected, scan_for_symbols(&input));
		}

		#[test]
		fn only_digits() {
			let input = "0".lines().collect();
			let expected: Vec<(usize, usize)> = vec![];

			assert_eq!(expected, scan_for_symbols(&input));
		}

		#[test]
		fn empty_neighbor() {
			let input = ".*".lines().collect();
			let expected = vec![(1, 0)];

			assert_eq!(expected, scan_for_symbols(&input));
		}

		#[test]
		fn digit_neighbor() {
			let input = "0*".lines().collect();
			let expected = vec![(1, 0)];

			assert_eq!(expected, scan_for_symbols(&input));
		}

		#[test]
		fn all_blank() {
			let input = "...\n.*.\n...".lines().collect();
			let expected = vec![(1, 1)];

			assert_eq!(expected, scan_for_symbols(&input));
		}

		#[test]
		fn all_digits() {
			let input = "012\n3*5\n678".lines().collect();
			let expected = vec![(1, 1)];

			assert_eq!(expected, scan_for_symbols(&input));
		}

		#[test]
		fn adjacent_symbols() {
			let input = "...\n.*.\n.*.\n...".lines().collect();
			let expected = vec![(1, 1), (1, 2)];

			assert_eq!(expected, scan_for_symbols(&input));
		}
	}

	mod get_neighbors {
		use super::*;

		#[test]
		fn all_neighbors() {
			let coords = vec![(1, 1)];
			let bounds = (3, 3);

			let expected = vec![
				(0, 0),
				(0, 1),
				(0, 2),
				(1, 0),
				(1, 2),
				(2, 0),
				(2, 1),
				(2, 2),
			];

			assert_eq!(expected, get_neighbors(&coords, bounds));
		}

		#[test]
		fn bounded() {
			let coords = vec![(1, 1)];
			let bounds = (2, 2);

			let expected = vec![(0, 0), (0, 1), (1, 0)];

			assert_eq!(expected, get_neighbors(&coords, bounds));
		}

		#[test]
		fn corner() {
			let coords = vec![(0, 0)];
			let bounds = (3, 3);

			let expected = vec![(0, 1), (1, 0), (1, 1)];

			assert_eq!(expected, get_neighbors(&coords, bounds));
		}
	}

	mod scan_for_numbers {
		use super::*;

		#[test]
		fn no_numbers() {
			let input = ".".lines().collect();
			let num_queue = vec![(0, 0)];

			let expected: Vec<usize> = vec![];

			assert_eq!(expected, scan_for_numbers(&input, num_queue));
		}

		#[test]
		fn one_digit() {
			let input = "1".lines().collect();
			let num_queue = vec![(0, 0)];

			let expected: Vec<usize> = vec![1];

			assert_eq!(expected, scan_for_numbers(&input, num_queue));
		}

		#[test]
		fn two_digits() {
			let input = "12".lines().collect();
			let num_queue = vec![(0, 0)];

			let expected: Vec<usize> = vec![12];

			assert_eq!(expected, scan_for_numbers(&input, num_queue));
		}

		#[test]
		fn dedup() {
			let input = "12".lines().collect();
			let num_queue = vec![(0, 0), (1, 0)];

			let expected: Vec<usize> = vec![12];

			assert_eq!(expected, scan_for_numbers(&input, num_queue));
		}

		#[test]
		fn two_numbers() {
			let input = "1.2".lines().collect();
			let num_queue = vec![(0, 0), (2, 0)];

			let expected: Vec<usize> = vec![1, 2];

			assert_eq!(expected, scan_for_numbers(&input, num_queue));
		}

		#[test]
		fn not_queued_left() {
			let input = "1.2".lines().collect();
			let num_queue = vec![(2, 0)];

			let expected: Vec<usize> = vec![2];

			assert_eq!(expected, scan_for_numbers(&input, num_queue));
		}

		#[test]
		fn not_queued_right() {
			let input = "1.2".lines().collect();
			let num_queue = vec![(0, 0)];

			let expected: Vec<usize> = vec![1];

			assert_eq!(expected, scan_for_numbers(&input, num_queue));
		}
	}

	#[test]
	#[ignore]
	fn test_part2() {
		let input = todo!();
		let expected = "";

		assert_eq!(expected, part2(input));
	}
}
