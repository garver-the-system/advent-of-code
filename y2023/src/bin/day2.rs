use y2023::*;

fn main() {
	let input = include_str!("../../input/d2/input.txt");

	print_section("Part 1", &part1(input));
	print_section("Part 2", &part2(input));
}

fn part1(input: &str) -> String {
	let mut sum = 0;
	for line in input.lines() {
		let mut parts = line.split(":");
		let game_number = parts
			.next()
			.unwrap()
			.split_whitespace()
			.last()
			.unwrap()
			.parse::<usize>()
			.unwrap();

		let mut limit_exceeded = false;

		// Check each handful, continue if a condition is broken.
		for hand in parts.next().unwrap().split(";") {
			for color_and_count in hand.split(",") {
				let mut iter = color_and_count.split_whitespace();
				let count = iter.next().unwrap().parse::<usize>().unwrap();
				let color = iter.next().unwrap();
				let limit = match color {
					"red" => 12,
					"green" => 13,
					"blue" => 14,
					_ => panic!("Unknown color: {}", color),
				};

				if count > limit {
					limit_exceeded = true;
					break;
				}
			}
			if limit_exceeded {
				break;
			}
		}

		if !limit_exceeded {
			sum += game_number;
		}
	}
	return sum.to_string();
}

fn part2(input: &str) -> String {
	let mut sum = 0;
	for line in input.lines() {
		let mut parts = line.split(":");
		// We need to skip the game number.
		let _game = parts.next();

		let (mut red_max, mut green_max, mut blue_max) = (0, 0, 0);

		// Check each handful, continue if a condition is broken.
		for hand in parts.next().unwrap().split(";") {
			for color_and_count in hand.split(",") {
				let mut iter = color_and_count.split_whitespace();
				let count = iter.next().unwrap().parse::<usize>().unwrap();
				let color = iter.next().unwrap();
				match color {
					"red" => red_max = if count > red_max { count } else { red_max },
					"green" => green_max = if count > green_max { count } else { green_max },
					"blue" => blue_max = if count > blue_max { count } else { blue_max },
					_ => panic!("Unknown color: {}", color),
				};
			}
		}

		let power = red_max * green_max * blue_max;
		sum += power;
	}
	return sum.to_string();
}

#[cfg(test)]
mod test {
	use super::*;

	#[test]
	fn test_part1() {
		let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n\
		Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\n\
		Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\n\
		Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\n\
		Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

		let expected = "8";

		assert_eq!(expected, part1(input));
	}

	#[test]
	fn test_part2() {
		let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n\
		Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\n\
		Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\n\
		Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\n\
		Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

		let expected = "2286";

		assert_eq!(expected, part2(input));
	}
}
