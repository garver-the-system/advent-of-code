use std::collections::HashMap;

use y2023::*;

fn main() {
	let input = include_str!("../../input/d1/input.txt");

	print_section("Part 1", &part1(input));
	print_section("Part 2", &part2(input));
}

fn part1(input: &str) -> String {
	let digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
	let mut sum = 0;

	for line in input.lines() {
		let chars: Vec<char> = line.chars().filter(|c| digits.contains(c)).collect();
		let cal = format!("{}{}", chars.first().unwrap(), chars.last().unwrap());
		sum += cal.parse::<usize>().unwrap();
	}

	sum.to_string()
}

fn part2(input: &str) -> String {
	let digits = [
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "one", "two", "three", "four", "five",
		"six", "seven", "eight", "nine",
	];
	let rev_digits = [
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "eno", "owt", "eerht", "ruof", "evif",
		"xis", "neves", "thgie", "enin",
	];
	let mut sum = 0;

	for line in input.lines() {

		let d1 = find_match(line, &digits);

		let rev_line = line.chars().rev().collect::<String>();
		let d2 = find_match(&rev_line, &rev_digits);

		let cal = format!("{d1}{d2}");
		sum += cal.parse::<usize>().unwrap();
	}

	sum.to_string()
}

fn find_match(input: &str, patterns: &[&str]) -> String {

	let map_digit = HashMap::from([
		("0", "0"),
		("1", "1"),
		("2", "2"),
		("3", "3"),
		("4", "4"),
		("5", "5"),
		("6", "6"),
		("7", "7"),
		("8", "8"),
		("9", "9"),
		("one", "1"),
		("two", "2"),
		("three", "3"),
		("four", "4"),
		("five", "5"),
		("six", "6"),
		("seven", "7"),
		("eight", "8"),
		("nine", "9"),
		("eno", "1"),
		("owt", "2"),
		("eerht", "3"),
		("ruof", "4"),
		("evif", "5"),
		("xis", "6"),
		("neves", "7"),
		("thgie", "8"),
		("enin", "9"),
	]);

	let input = format!("{input}    ");
	let mut window = Vec::new();

	for char in input.chars() {
		window.push(char);
		if window.len() < 5 {
			continue;
		} else if window.len() > 5 {
			window.remove(0);
		}

		let word = String::from_iter(window.iter());
		for pattern in patterns {
			if word.starts_with(pattern) {
				return map_digit[pattern].to_string();
			}
		}
	}
	panic!()
}

#[cfg(test)]
mod test {
	use super::*;

	#[test]
	fn test_part1() {
		let input = "1abc2\n\
			pqr3stu8vwx\n\
			a1b2c3d4e5f\n\
			treb7uchet";

		let expected = "142";

		assert_eq!(part1(input), expected);
	}

	#[test]
	fn test_part2() {
		let input = "two1nine\n\
			eightwothree\n\
			abcone2threexyz\n\
			xtwone3four\n\
			4nineeightseven2\n\
			zoneight234\n\
			7pqrstsixteen";

		let expected = "281";

		assert_eq!(part2(input), expected);
	}

	#[test]
	fn part2_regex_single_match() {
		let input = "one";
		let expected = "11";

		assert_eq!(part2(input), expected);
	}

	#[test]
	fn part2_zero_str() {
		let input = "zero1zero";
		let expected = "11";

		assert_eq!(part2(input), expected);
	}

	#[test]
	fn part2_overlapping_match() {
		let input = "eightwo";
		let expected = "82";

		assert_eq!(part2(input), expected);

		let input = "178ncllbfkkh4eightwoq";
		let expected = "12";

		assert_eq!(part2(input), expected);
	}
}
