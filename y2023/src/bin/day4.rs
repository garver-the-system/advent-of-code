use std::collections::VecDeque;

use y2023::*;

fn main() {
	let input = include_str!("../../input/d4/input.txt");

	print_section("Part 1", &part1(input));
	print_section("Part 2", &part2(input));
}

fn part1(input: &str) -> String {
	let mut sum = 0;

	for line in input.lines() {
		let mut lists = line.split(':').last().unwrap().split('|');
		let winners = list_to_nums(&lists.next().unwrap());
		let numbers = list_to_nums(&lists.next().unwrap());

		let mut points = 0;
		for num in numbers {
			if winners.contains(&num) {
				points = std::cmp::max(1, points * 2);
			}
		}

		sum += points;
	}

	sum.to_string()
}

fn list_to_nums(list: &str) -> Vec<usize> {
	list.split_whitespace()
		.map(|n| n.parse().unwrap())
		.collect()
}

fn part2(input: &str) -> String {
	let mut copies_queue = CopyQueue::new();

	for line in input.lines() {
		let copies = copies_queue.pop();

		let mut lists = line.split(':').last().unwrap().split('|');
		let winners = list_to_nums(&lists.next().unwrap());
		let numbers = list_to_nums(&lists.next().unwrap());

		let mut count = 0;
		for num in numbers {
			if winners.contains(&num) {
				count += 1;
			}
		}

		copies_queue.push(count, copies);
	}

	copies_queue.sum.to_string()
}

#[derive(Debug, PartialEq)]
struct CopyQueue {
	queue: VecDeque<usize>,
	sum: usize,
}

impl CopyQueue {
	pub fn new() -> Self {
		Self {
			queue: VecDeque::new(),
			sum: 0,
		}
	}

	pub fn push(&mut self, mut count: usize, copies: usize) {
		if count != 0 {
			for card in self.queue.iter_mut() {
				*card += copies;

				count -= 1;
				if count == 0 {
					break;
				}
			}
		}

		let mut app = VecDeque::from(vec![1 + copies; count]);
		self.queue.append(&mut app);
	}

	pub fn pop(&mut self) -> usize {
		let copies = self.queue.pop_front().unwrap_or(1);
		self.sum += copies;
		copies
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	mod part1 {
		use super::*;

		#[test]
		fn example() {
			let input = "\
				Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53\n\
				Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19\n\
				Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1\n\
				Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83\n\
				Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36\n\
				Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11\n\
			";
			let expected = "13";

			assert_eq!(expected, part1(input));
		}

		mod list_to_nums {
			use super::*;

			#[test]
			fn one_number() {
				let input = "  1";
				let expected = VecDeque::from(vec![1]);

				assert_eq!(expected, list_to_nums(input));
			}

			#[test]
			fn two_numbers() {
				let input = "  1  2";
				let expected = VecDeque::from(vec![1, 2]);

				assert_eq!(expected, list_to_nums(input));
			}

			#[test]
			fn multiple_digits() {
				let input = " 12 34";
				let expected = VecDeque::from(vec![12, 34]);

				assert_eq!(expected, list_to_nums(input));
			}
		}
	}

	#[test]
	fn test_part2() {
		let input = "\
				Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53\n\
				Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19\n\
				Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1\n\
				Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83\n\
				Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36\n\
				Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11\n\
			";
		let expected = "30";

		assert_eq!(expected, part2(input));
	}

	mod part2 {
		use super::*;

		mod copy_queue {
			use super::*;

			#[test]
			fn new() {
				let expected = CopyQueue {
					queue: VecDeque::new(),
					sum: 0,
				};

				assert_eq!(expected, CopyQueue::new());
			}

			mod push {
				use super::*;

				#[test]
				fn single_copy() {
					let expected = CopyQueue {
						queue: VecDeque::from(vec![2]),
						sum: 0,
					};

					let mut copies = CopyQueue::new();
					copies.push(1, 1);

					assert_eq!(expected, copies);
				}

				#[test]
				fn copies() {
					let expected = CopyQueue {
						queue: VecDeque::from(vec![5]),
						sum: 0,
					};

					let mut copies = CopyQueue::new();
					copies.push(1, 2);
					copies.push(1, 2);

					assert_eq!(expected, copies);
				}

				#[test]
				fn count() {
					let expected = CopyQueue {
						queue: VecDeque::from(vec![2, 2]),
						sum: 0,
					};

					let mut copies = CopyQueue::new();
					copies.push(2, 1);

					assert_eq!(expected, copies);
				}

				#[test]
				fn counts() {
					let expected = CopyQueue {
						queue: VecDeque::from(vec![4, 3]),
						sum: 0,
					};

					let mut copies = CopyQueue::new();
					copies.push(2, 2);
					copies.push(1, 1);

					assert_eq!(expected, copies);
				}

				#[test]
				fn large_count() {
					let expected = CopyQueue {
						queue: VecDeque::from(vec![2, 2, 2, 2, 2, 2, 2]),
						sum: 0,
					};
					let mut copies = CopyQueue::new();
					copies.push(7, 1);

					assert_eq!(expected, copies);
				}

				#[test]
				fn large_then_small_count() {
					let expected = CopyQueue {
						queue: VecDeque::from(vec![3, 3, 2, 2, 2, 2, 2]),
						sum: 0,
					};
					let mut copies = CopyQueue::new();
					copies.push(7, 1);
					copies.push(2, 1);

					assert_eq!(expected, copies);
				}

				#[test]
				fn zero_count() {
					let expected = CopyQueue {
						queue: VecDeque::new(),
						sum: 0,
					};
					let mut copies = CopyQueue::new();
					copies.push(0, 1);

					assert_eq!(expected, copies);
				}

				#[test]
				fn large_then_zero_count() {
					let expected = CopyQueue {
						queue: VecDeque::from(vec![2, 2, 2, 2, 2, 2, 2]),
						sum: 0,
					};
					let mut copies = CopyQueue::new();
					copies.push(7, 1);
					copies.push(0, 1);

					assert_eq!(expected, copies);
				}
			}

			mod pop {
				use super::*;

				#[test]
				fn single_copy() {
					let expected = CopyQueue {
						queue: VecDeque::from(vec![2]),
						sum: 2,
					};

					let mut copies = CopyQueue::new();
					copies.push(2, 1);
					let popped = copies.pop();

					assert_eq!(2, popped);
					assert_eq!(expected, copies);
				}

				#[test]
				fn copies() {
					let expected = CopyQueue {
						queue: VecDeque::from(vec![3]),
						sum: 5,
					};

					let mut copies = CopyQueue::new();
					copies.push(1, 2);
					copies.push(2, 2);
					let popped = copies.pop();

					assert_eq!(5, popped);
					assert_eq!(expected, copies);
				}

				#[test]
				fn empty() {
					let expected = CopyQueue {
						queue: VecDeque::new(),
						sum: 1,
					};

					let mut copies = CopyQueue::new();
					let popped = copies.pop();

					assert_eq!(1, popped);
					assert_eq!(expected, copies);
				}
			}
		}
	}
}
